<?php
Class ProjectNameNumber
 {
    public function checkIfIsInteger( $number ) {
        if ( is_numeric( $number ) ) {
            return true;
        } else {
            return false;
        }
    }

    public function checkIsLowerThan1( $number ) {
        if ( $number > 1 ) {
            return true;
        } else {
            return false;
        }
    }

    public function checkIfLargerThan10000( $number ) {
        if ( $number < 10000 ) {
            return true;
        } else {
            return false;
        }
    }
}
