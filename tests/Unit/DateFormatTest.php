<?php

//namespace Tests\Unit;
require 'Date.php';

use PHPUnit\Framework\TestCase;

class DateFormatTest extends TestCase
{
    /**
     * A basic test example.
     *test
     * @return void
     */

    public function testDateFormat()
    {
        $date = new Date();
        $this->assertTrue($date->checkDateFormat('2020-10-10')); //jei daugiau arba lygu 10 true
        $this->assertFalse($date->checkDateFormat('2020-10-9')); //jei maziau nei 10 true
    }
    public function testDateInterval()
    {
        $date = new Date();
        $this->assertTrue($date->checkDateInterval('2020-10-10')); //jei patenka i intervala true
        $this->assertFalse($date->checkDateFormat('1979-10-9')); //jei nepatenka i intervala true
    }
    public function testIsAllNumbers()
    {
        $date = new Date();
        $this->assertTrue($date->checkDateIsInteger('2020-10-10')); //jei visi skaiciai true
        $this->assertFalse($date->checkDateIsInteger('2a20-1l-10')); //jei nevisi skaiciai true
    }
}
